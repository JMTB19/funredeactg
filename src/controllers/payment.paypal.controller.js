const config = require('../config/config.js');
const logger = require('../logs/logger.js');

const params = new URLSearchParams();
params.append('granted_type', 'client_credentials');

const createPay = async (req, res) => {
    try{
        const pay = {
            intent: 'CAPTURE',
            punchase_units: [
                {
                    amount: {
                        currency_code: 'USD',
                        value: '100.00',
                    }
                }
            ],
            application_context: {
                brand_name: 'FUNDACION RETOÑOS DE AMOR CTG',
                landing_page: 'NO_PREFERENCE',
                user_action: 'PAY_NOW',
                return_url: `${config.host}:${config.port}/paypal-approved`,
                cancel_url: `${config.host}:${config.port}/paypayl-cancel`,
            }
        };
        logger.info('SOLICITANDO TOKEN DE AUTORIZACION'.gray);
        const { data: {access_token}} = await fetch(`${config.paypalURL}/v1/oauth2/token`, params, {
            auth: {
                username: config.paypalID,
                password: config.paypalKEY,
            }
        });
        if(!access_token){
            logger.warn('TOKEN NO RECIBIDO!!!'.yellow);
            return res.status(403).json({msg: 'TOKEN NO RECIBIDO'});
        }
        logger.warn('TOKEN RECIBIDO!!!'.green);
        const resp = await fetch(`${config.paypalURL}/v2/checkout/orders`, pay, {
            headers: {
                'CONTENT_TYPE': 'application/json',
                Autorization: `Bearer ${access_token}`,
            }
        }); 
        logger.info('DONACION CREADA!!!!'.green);
        return res.status(200).json({msg: 'DONACION CREADA!!!!', data: resp.data});
    }catch(err){
        //!: capturando cualquier error al realizar donacion atraves de PAYPAL   
        logger.error(`ERROR DEL SERVIDOR: ${err.message}`.red);
        return res.status(500).json({msg: 'ERROR INTERNO DEL SERVIDOR'});
    }
};

const approvePay = async (req, res) => {
    try{
        const { token } = req.query;
        const resp = await fetch(`${config.paypalURL}/v2/checkout/orders/${token}/capture`, {}, {
            auth: {
                username: config.paypalID,
                password: config.paypalKEY,
            }

        });
        if(!resp){
            logger.warn('PAGO NO RECIBIDO!!!'.yellow);
            return res.status(403).json({msg: 'PAGO NO RECIBIDO'}); 
        }
        logger.info('DONACION EXITOSA!!!!'.green);
        return res.status(200).json({msg: 'DONACION EXITOSA!!!!', data: resp});
    }catch(err){
        //!: capturando cualquier error al realizar donacion atraves de PAYPAL  
        logger.error(`ERROR DEL SERVIDOR: ${err.message}`.red);
        return res.status(500).json({msg: 'ERROR INTERNO DEL SERVIDOR'}); 
    }
};