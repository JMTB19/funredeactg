const config = require('../config/config.js');
const logger = require('../logs/logger.js');

const params = new URLSearchParams();
params.append('granted_type', 'client_credentials');

 //*: cabeceras necesarias para peticiones a la api de nequi
const headers = {
    'CONTENT_TYPE': 'application/json',
    'Authorization': '',
    'x-api-key': `${config.nequiApiKey}`,
};
const tokenBasic = Buffer.from(`${config.nequiID}:${config.nequiKEY}`).toString('base64');

const createPayNequi = async ( req, res) => {
    try{
        //todo: solicitar el token de acceso a la api de nequi
        const data = await fetch(config.nequiURLAUTH, params, {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Authorization': `Basic ${tokenBasic}`,
            },
        });
        //?: actualizando el token de acceso a la api de nequi
        headers['Authorization'] =  `Bearer ${data.accessToken}`;
        const resp = await fetch(`${config.nequiURL}/payments/v2/-services-paymentservice-generatecodeqr`, {
            headers: headers,
        }); 
    }catch(err){
        //!: capturando cualquier error al realizar donacion atraves de PAYPAL  
        logger.error(`ERROR DEL SERVIDOR: ${err.message}`.red);
        return res.status(500).json({msg: 'ERROR INTERNO DEL SERVIDOR'}); 
    }
};

const statusPayNequi = async ( req, res) => {
    try{
        //todo: solicitar el token de acceso a la api de nequi
        const data = await fetch(config.nequiURLAUTH, params, {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Authorization': `Basic ${tokenBasic}`,
            },
        });
        //?: actualizando el token de acceso a la api de nequi
        headers['Authorization'] =  `Bearer ${data.accessToken}`;
        const resp = await fetch(`${config.nequiURL}/payments/v2/-services-paymentservice-getstatuspayment`, {
            headers: headers,
        }); 
    }catch(err){
        //!: capturando cualquier error al realizar donacion atraves de PAYPAL  
        logger.error(`ERROR DEL SERVIDOR: ${err.message}`.red);
        return res.status(500).json({msg: 'ERROR INTERNO DEL SERVIDOR'}); 
    }
};