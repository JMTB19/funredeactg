const { PrismaClient } = require('@prisma/client');
const logger = require('../logs/logger.js');

//*: inicializando el cliente de prisma
const prisma = new PrismaClient();

//todo: controlador para la consultas de todos los usuarios
const getUser = async (req, res) => {
    try{
        await prisma.$connect();
        const users = await prisma.user.findMany({
            include: {
                kids: true,
            }
        });
        if(!users){
            //?: si no exite la variable al realizar la consulta a la DB
            logger.error('USERS NOT FOUND!!!'.yellow);
            return res.status(400).json({msg: 'USERS NOT FOUND!!!'});
        }
        logger.info('USERS FOUND!!!'.green);
        await prisma.$disconnect();
        return res.status(200).json({msg: 'USERS FOUND!!!', data: users});
    }catch(err){
        //!: capturando cualquier error al realizar la consulta a la DB
        logger.error(`ERROR DEL SERVIDOR: ${err.message}`.red);
        await prisma.$disconnect();
        return res.status(500).json({msg: 'ERROR INTERNO DEL SERVIDOR'});
    }
};
//todo: controlador para la creacion de un usuario
const postUser = async (req, res) => {
    try{
        await prisma.$connect();
        const { email, name, phone, password } = req.body;
        const user = await prisma.user.create({
            data: { 
                email: email, 
                name: name, 
                phone: phone, 
                password: password, 
            }
        });
        if(!user){
            //?: si no exite la variable al realizar la consulta a la DB
            logger.warn('USER NOT CREATED!!!'.yellow);
            return res.status(400).json({msg: 'USER NOT CREATED!!!'});
        }
        logger.info('USER CREATED!!!'.green);
        await prisma.$disconnect();
        return res.status(200).json({msg: 'USER CREATED!!!', data: user});
    }catch(err){ 
        //!: capturando cualquier error al realizar la consulta a la DB  
        logger.error(`ERROR DEL SERVIDOR: ${err.message}`.red);
        await prisma.$disconnect();
        return res.status(500).json({msg: 'ERROR INTERNO DEL SERVIDOR'});
    }
};
//todo: controlador para actualizar un usuario
const updateUser = async (req, res) => {
    try{
        await prisma.$connect();
        const { email, name, phone, password } = req.body;
        const user = await prisma.user.update({
            data:{
                email: email, 
                name: name, 
                phone: phone, 
                password: password, 
            }
        });
        if(!user){
            //?: si no exite la variable al realizar la consulta a la DB
            logger.warn('USER NOT UPDATED!!!'.yellow);
            return res.status(400).json({msg: 'USER NOT UPDATE!!!'});
        }
        logger.info('USER UPDATED!!!'.green);
        await prisma.$disconnect();
        return res.status(200).json({msg: 'USER UPDATED!!!', data: user});
    }catch(err){
        //!: capturando cualquier error al realizar la consulta a la DB  
        logger.error(`ERROR DEL SERVIDOR: ${err.message}`.red);
        await prisma.$disconnect();
        return res.status(500).json({msg: 'ERROR INTERNO DEL SERVIDOR'});
    }
};
//todo: controlador para eliminar un usuario
const deleteUser = async (req, res) => {
    try{
        await prisma.$connect();
        const email= req.body.email;
        const user = await prisma.user.delete({
            where: {
                email:email,
            }
        });
        if(!user){
            //?: si no exite la variable al realizar la consulta a la DB
            logger.warn('USER NOT DELETED!!!'.yellow);
            return res.status(400).json({msg: 'USER NOT DELETED!!!'});
        }
        logger.info('USER DELETED!!!'.green);
        await prisma.$disconnect();
        return res.status(200).json({msg: 'USER DELETED!!!', data: user});
    }catch(err){
        //!: capturando cualquier error al realizar la consulta a la DB  
        logger.error(`ERROR DEL SERVIDOR: ${err.message}`.red);
        await prisma.$disconnect();
        return res.status(500).json({msg: 'ERROR INTERNO DEL SERVIDOR'});
    }
};

module.exports = {
    getUser,
    postUser,
    updateUser,
    deleteUser,
};


