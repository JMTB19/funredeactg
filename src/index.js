const app = require('./app.js');
const logger = require('./logs/logger.js');
const config = require('./config/config.js');


app.listen(config.port, () => {
    logger.info('listening on port: ' + config.port);
});





