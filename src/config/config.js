const dotenv = require('dotenv');
dotenv.config();

const config = {
    port: process.env.PORT_DEV || 4244,
    host: process.env.HOST_DEV || 'localhost',
    paypalID: process.env.PAYPAL_ID,
    paypalKEY: process.env.PAYPAL_KEY,
    paypalURL: process.env.PAYPAL_URL,
    nequiURLAUTH: process.env.NEQUI_URL_AUTH,
    nequiID: process.env.NEQUI_ID,
    nequiKEY: process.env.NEQUI_KEY,
    nequiApiKey: process.env.NEQUI_API_KEY,
    nequiURL: process.env.NEQUI_URL,
}

module.exports = config;