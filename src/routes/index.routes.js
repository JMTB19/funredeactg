const express = require('express');
const router = express.Router();

router.get('/profile');

router.post('/login');

router.get('/logout');

router.post('/register');

router.post('/donate');


module.exports = router;