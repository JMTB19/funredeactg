const express = require('express');
const { getUser, postUser, updateUser, deleteUser } = require('../controllers/user.controller.js');
//*: inicializando el router del framework de express
const router = express.Router();
//todo: creando las rutas de acceso
router.get('/user', getUser);

router.post('/user', postUser);

router.put('/user', updateUser);

router.delete('/user', deleteUser);

module.exports = router;