const express = require('express');
const router = express.Router();

router.post('/paypal');

router.get('/paypal-approved');

router.get('/paypal-cancel');

router.post('/nequi');

router.get('/nequi-status');

router.get('/nequi-cancel');

module.exports = router;