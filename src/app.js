const express = require('express');
const logger = require('./logs/logger.js');
const morgan = require('morgan');
const cors = require('cors');
const userRoutes = require('./routes/user.routes.js');
const kidRoutes = require('./routes/kid.routes.js');
const indexRoutes = require('./routes/index.routes.js');
//*: inicialiando el servidor de express
const app = express();

//todo: utilizando middlewares
app.use(cors());
app.use(morgan('dev'));
app.use(express.json());  

//todo: utilizando las rutas creadas
app.get('/', (req, res) => {
    res.send('HELLO WORLD!!!!');
});
app.use('/api', userRoutes);
app.use('/api', kidRoutes);
app.use('/api', indexRoutes);


module.exports = app;