const config = require('./src/config/config.js');

const params = new URLSearchParams();
params.append('granted_type', 'client_credentials');
const tokenBasic = Buffer.from(`${config.nequiID}:${config.nequiKEY}`).toString('base64');

async function name (){
    
console.log('basic: ',tokenBasic);

const data = await fetch(config.nequiURLAUTH, params, {
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': `Basic ${tokenBasic}`,
    },
});
console.dir(data);
};

name();

